<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @var ArrayCollection|Contact[]
	 * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="user", cascade={"persist"})
	 */
	protected $contacts;

	/**
	 * User constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->roles    = ["ROLE_USER"];
		$this->contacts = new ArrayCollection();
	}

	/**
	 * @return Contact[]|ArrayCollection
	 */
	public function getContacts()
	{
		return $this->contacts;
	}

	/**
	 * @param Contact[]|ArrayCollection $contacts
	 *
	 * @return $this
	 */
	public function setContacts($contacts)
	{
		$this->contacts = $contacts;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return $this
	 */
	public function setId(?int $id)
	{
		$this->id = $id;
		return $this;
	}
}
