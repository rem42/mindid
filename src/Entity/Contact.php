<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 * @Vich\Uploadable()
 * @Assert\Expression(expression="this.getPhone() != null or this.getMobile() != null", message="Veuillez indiquer au moins un numéro de téléphone")
 */
class Contact
{
	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Veuillez indiquer un prénom")
	 */
	protected $firstName;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Veuillez indiquer un nom")
	 */
	protected $lastName;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Veuillez indiquer un mail")
	 */
	protected $mail;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $phone;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $mobile;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Veuillez indiquer un code postal")
	 */
	protected $postalCode;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Veuillez indiquer une ville")
	 */
	protected $city;

	/**
	 * NOTE: This is not a mapped field of entity metadata, just a simple property.
	 *
	 * @Vich\UploadableField(mapping="contact_photo", fileNameProperty="image.name", size="image.size", mimeType="image.mimeType", originalName="image.originalName", dimensions="image.dimensions")
	 *
	 * @var File
	 */
	protected $imageFile;

	/**
	 * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
	 *
	 * @var EmbeddedFile
	 */
	protected $image;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $updatedAt;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="contacts")
	 * @ORM\JoinColumn()
	 */
	protected $user;

	/**
	 * Contact constructor.
	 */
	public function __construct()
	{
		$this->image     = new EmbeddedFile();
		$this->createdAt = new \DateTime();
	}

	/**
	 * @return File
	 */
	public function getImageFile(): ?File
	{
		return $this->imageFile;
	}

	/**
	 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
	 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
	 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
	 * must be able to accept an instance of 'File' as the bundle will inject one here
	 * during Doctrine hydration.
	 *
	 * @param File|UploadedFile $image
	 *
	 * @throws \Exception
	 */
	public function setImageFile(?File $image = null)
	{
		$this->imageFile = $image;

		if (null !== $image) {
			// It is required that at least one field changes if you are using doctrine
			// otherwise the event listeners won't be called and the file is lost
			$this->updatedAt = new \DateTimeImmutable();
		}
	}

	/**
	 * @return int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return $this
	 */
	public function setId(?int $id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFirstName(): ?string
	{
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 *
	 * @return $this
	 */
	public function setFirstName(?string $firstName)
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName(): ?string
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 *
	 * @return $this
	 */
	public function setLastName(?string $lastName)
	{
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMail(): ?string
	{
		return $this->mail;
	}

	/**
	 * @param string $mail
	 *
	 * @return $this
	 */
	public function setMail(?string $mail)
	{
		$this->mail = $mail;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPhone(): ?string
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 *
	 * @return $this
	 */
	public function setPhone(?string $phone)
	{
		$this->phone = $phone;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMobile(): ?string
	{
		return $this->mobile;
	}

	/**
	 * @param string $mobile
	 *
	 * @return $this
	 */
	public function setMobile(?string $mobile)
	{
		$this->mobile = $mobile;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPostalCode(): ?string
	{
		return $this->postalCode;
	}

	/**
	 * @param string $postalCode
	 *
	 * @return $this
	 */
	public function setPostalCode(?string $postalCode)
	{
		$this->postalCode = $postalCode;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCity(): ?string
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 *
	 * @return $this
	 */
	public function setCity(?string $city)
	{
		$this->city = $city;
		return $this;
	}

	/**
	 * @return EmbeddedFile
	 */
	public function getImage(): ?EmbeddedFile
	{
		return $this->image;
	}

	/**
	 * @param EmbeddedFile $image
	 *
	 * @return $this
	 */
	public function setImage(?EmbeddedFile $image)
	{
		$this->image = $image;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdatedAt(): ?\DateTime
	{
		return $this->updatedAt;
	}

	/**
	 * @param \DateTime $updatedAt
	 *
	 * @return $this
	 */
	public function setUpdatedAt(?\DateTime $updatedAt)
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser(): ?User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 *
	 * @return $this
	 */
	public function setUser(?User $user)
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): ?\DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return $this
	 */
	public function setCreatedAt(?\DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}
}
