<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PostalCodeStatsCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('mindid:postalcode')
			->setDescription('Get Contact stats')
		;
	}

	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return int|null|void
	 * @throws \Doctrine\DBAL\DBALException
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$contactRepository = $this->getContainer()->get('app.repository.contact');

		foreach ($contactRepository->numberOfContactByPostalCode() as $item) {
			$output->writeln($item['department'] . " : " . $item["total"] . " contact(s)");
		}
		$output->writeln(sizeof($contactRepository->findByCreatedToday()) . " contact(s) ajouté aujourd'hui");
	}
}
