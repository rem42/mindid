<?php

namespace App\Form;


use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ContactAddType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstName', TextType::class)
			->add('lastName', TextType::class)
			->add('mail', EmailType::class)
			->add('phone', TelType::class, [
				'required' => false,
			])
			->add('mobile', TelType::class, [
				'required' => false,
			])
			->add('postalCode', TextType::class)
			->add('city', TextType::class)
			->add('imageFile', VichImageType::class, [
				'allow_delete' => true,
				'download_label' => '...',
				'download_uri' => true,
				'image_uri' => true,
				'imagine_pattern' => '...',
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Contact::class,
		]);
	}
}