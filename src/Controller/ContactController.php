<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactAddType;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @package App\Controller
 *
 * @Route("/contact")
 */
class ContactController extends Controller
{
	/**
	 * @Route("/add", name="contact_add")
	 *
	 * @param Request           $request
	 * @param ContactRepository $contactRepository
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function index(Request $request, ContactRepository $contactRepository)
	{
		$contact = new Contact();
		$form    = $this->createForm(ContactAddType::class, $contact);

		$form->handleRequest($request);
		if ($form->isSubmitted()) {
			$sameMailContact = $contactRepository->findOneBy([
				'user' => $this->getUser(),
				'mail' => $contact->getMail(),
			]);
			if ($sameMailContact instanceof Contact) {
				$form->addError(new FormError("Cette adresse mail est déjà utilisée"));
			}

			if ($form->isValid()) {
				$contact->setUser($this->getUser());
				$contactRepository->save($contact);

				return $this->redirectToRoute('contact_add');
			}
		}

		return $this->render('contact/add.html.twig', [
			'form' => $form->createView(),
		]);
	}
}
