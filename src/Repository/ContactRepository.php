<?php

namespace App\Repository;

use App\Entity\Contact;
use App\Manager\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ContactRepository
 * @package App\Repository
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[] findAll()
 * @method Contact[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends EntityManager
{
	/**
	 * UserRepository constructor.
	 *
	 * @param RegistryInterface $registry
	 */
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Contact::class);
	}

	/**
	 * @return array
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function numberOfContactByPostalCode()
	{
		$sql = "
			SELECT COUNT(*) as total, SUBSTRING(c.postal_code, 1, 2) as department
			FROM contact c
			GROUP BY department
			ORDER BY department ASC
		";

		$conn = $this->_em->getConnection();
		$rows = $conn->prepare($sql);
		$rows->execute();

		return $rows->fetchAll();
	}

	/**
	 * @return Contact[]|null
	 */
	public function findByCreatedToday()
	{
		$dateTime = new \DateTime();
		return $this->createQueryBuilder('c')
			->where('c.createdAt >= :dateStart')
			->andWhere('c.createdAt <= :dateEnd')
			->setParameters([
				'dateStart' => $dateTime->format('Y-m-d') . " 00:00:00",
				'dateEnd' => $dateTime->format('Y-m-d') . " 23:59:59",
			])
			->getQuery()
			->getResult()
			;
	}
}
