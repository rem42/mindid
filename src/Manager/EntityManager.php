<?php

namespace App\Manager;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class EntityManager extends ServiceEntityRepository
{
	/**
	 * @param $object
	 */
	public function save($object)
	{
		$this->_em->persist($object);
		$this->_em->flush();
	}

	/**
	 * @param $array
	 */
	public function saveMultiple(array $array)
	{
		foreach ($array as $item) {
			$this->_em->persist($item);
		}
		$this->_em->flush();
	}

	/**
	 * @param $object
	 */
	public function refresh($object)
	{
		$this->_em->refresh($object);
	}

	/**
	 * @param $object
	 */
	public function remove($object)
	{
		$this->_em->remove($object);
		$this->_em->flush();
	}
}
