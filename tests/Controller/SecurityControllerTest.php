<?php

namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SecurityBundle\DataCollector\SecurityDataCollector;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityControllerTest extends WebTestCase
{
	/**
	 * Test with user credential
	 */
	public function testLogin()
	{
		$client            = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);
		$username          = 'test';
		$password          = 'test';
		$crawler           = $client->request('GET', '/login');
		$form              = $crawler->selectButton('login')->form();
		$form['_username'] = $username;
		$form['_password'] = $password;
		$client->enableProfiler();
		$client->submit($form);
		/** @var SecurityDataCollector $security */
		$security = $client->getProfile()->getCollector('security');
		$this->assertTrue(is_string($security->getUser()) && strlen($security->getUser()) > 0);
		$this->assertTrue($security->isAuthenticated(), 'Logged in user is not authenticated.');
	}

	/**
	 * Test without password
	 */
	public function testLoginWebNoPassword()
	{
		$client            = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);
		$username          = 'test';
		$password          = '';
		$url               = self::$container->get('router')->generate('login');
		$crawler           = $client->request('GET', $url);
		$form              = $crawler->selectButton('login')->form();
		$form['_username'] = $username;
		$form['_password'] = $password;
		$client->enableProfiler();
		$client->submit($form);
		/** @var SecurityDataCollector $security */
		$security = $client->getProfile()->getCollector('security');
		$this->assertFalse(is_string($security->getUser()) && strlen($security->getUser()) > 0);
		$this->assertFalse($security->isAuthenticated(), 'Logged in user is not authenticated.');
	}

	/**
	 * Test with token connection
	 */
	public function testLoginToken()
	{
		$client = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);

		$session = $client->getContainer()->get('session');

		$firewall = 'main';

		$token = new UsernamePasswordToken('admin', null, $firewall, ['ROLE_USER']);
		$session->set('_security_' . $firewall, serialize($token));
		$session->save();

		$cookie = new Cookie($session->getName(), $session->getId());
		$client->getCookieJar()->set($cookie);

		$url     = self::$container->get('router')->generate('contact_add');
		$crawler = $client->request('GET', $url);

		$this->assertTrue($client->getResponse()->isSuccessful());
		$this->assertGreaterThan(0, $crawler->filter('html:contains("Mail")')->count());
	}
}
