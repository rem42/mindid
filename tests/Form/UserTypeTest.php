<?php

namespace App\Tests\Form;

use App\Entity\Contact;
use App\Form\ContactAddType;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserTypeTest extends TypeTestCase
{
	/**
	 * @throws \Exception
	 */
	public function testSubmitValidData()
	{
		$formData = [
			'firstName' => 'Rémy',
			'lastName' => 'BRUYERE',
			'mail' => 'me@remy.ovh',
			'mobile' => '0666529870',
			'postalCode' => '74570',
			'city' => 'GROISY',
			'imageFile' => new UploadedFile(__DIR__ . '/../public/images/logo_header.png', 'logo_header.png'),
		];

		$objectToCompare = new Contact();

		$form = $this->factory->create(ContactAddType::class, $objectToCompare);

		$object = new Contact();
		$object
			->setFirstName($formData['firstName'])
			->setLastName($formData['lastName'])
			->setMail($formData['mail'])
			->setMobile($formData['mobile'])
			->setPostalCode($formData['postalCode'])
			->setCity($formData['city'])
			->setImageFile($formData['imageFile'])
		;

		$form->submit($formData);

		$this->assertTrue($form->isSynchronized());

		$this->assertEquals($object, $objectToCompare);

		$view     = $form->createView();
		$children = $view->children;

		foreach (array_keys($formData) as $key) {
			$this->assertArrayHasKey($key, $children);
		}
	}
}
