MindID Contact
==============

Base de données
---------------

La base de données ce trouve à la racine du projet : mindid.sql

Utilisation
-----------

Voici les 2 utilisateurs : 

    user: admin
    password: admin
    
    user: test
    password: test

Tache cron : php bin/console mindid:postalcode

Test
----

Je n'ai pas l'habitude de réaliser des tests unitaires.

Certains tests sont présents, mais je n'ai pas eux le temps de les finaliser.
